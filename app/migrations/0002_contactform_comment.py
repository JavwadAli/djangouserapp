# Generated by Django 3.0 on 2020-01-15 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contactform',
            name='comment',
            field=models.CharField(default='', max_length=500),
        ),
    ]
