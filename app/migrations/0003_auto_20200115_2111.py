# Generated by Django 3.0 on 2020-01-15 15:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_contactform_comment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contactform',
            name='comment',
            field=models.CharField(max_length=500),
        ),
    ]
